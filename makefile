test:
	@pytest --cov --cov-report term-missing

reset_test_db:
	@dropdb test_schedule; createdb test_schedule

reset_db:
	@dropdb schedule; createdb schedule
