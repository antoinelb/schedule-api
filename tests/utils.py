from typing import Any, Awaitable, Callable, TypeVar

T = TypeVar("T")


def as_async_fct(fct: Callable[..., T]) -> Callable[..., Awaitable[T]]:
    async def fct_(*args: Any, **kwargs: Any) -> T:
        return fct(*args, **kwargs)

    return fct_
