from datetime import datetime, timedelta, timezone
import json
from typing import Any, Dict, List

import pytest
from src.db import Event, PlannedEvent, RecurringEvent
from src.api.utils import convert_datetimes_for_json
from starlette.testclient import TestClient


async def test_get_events(
    client: TestClient,
    events: List[Event],
    auth_headers: Dict[str, str],
) -> None:
    resp = client.get("/events", headers=auth_headers)
    assert resp.status_code == 200
    data = resp.json()
    assert all(
        [
            d == convert_datetimes_for_json(await p.dict())
            for d, p in zip(data, events)
        ]
    )


async def test_add_event(
    client: TestClient, auth_headers: Dict[str, str]
) -> None:
    now = datetime.utcnow()
    resp = client.post(
        "/events/add",
        data=json.dumps(
            {"name": "test", "timestamp": int(now.timestamp())},
        ),
        headers=auth_headers,
    )
    assert resp.status_code == 200
    data = resp.json()
    assert data["name"] == "test"
    assert data["datetime"] == int(now.timestamp())

    await Event.filter(name="test").exists()


async def test_delete_event(
    client: TestClient,
    auth_headers: Dict[str, str],
    event: Event,
) -> None:
    id_ = event.id

    resp = client.post(
        "/events/delete",
        data=json.dumps({"id": id_}),
        headers=auth_headers,
    )
    assert resp.status_code == 200

    assert not await Event.filter(id=id_)


async def test_delete_event__doesnt_exist(
    client: TestClient,
    auth_headers: Dict[str, str],
) -> None:
    resp = client.post(
        "/events/delete",
        data=json.dumps({"id": 0}),
        headers=auth_headers,
    )
    assert resp.status_code == 400
    assert resp.text == "The event with id 0 doesn't exist."


async def test_get_planned_events(
    client: TestClient,
    planned_events: List[PlannedEvent],
    auth_headers: Dict[str, str],
) -> None:
    resp = client.get("/events/planned", headers=auth_headers)
    assert resp.status_code == 200
    data = resp.json()
    assert all(
        [
            d == convert_datetimes_for_json(await p.dict())
            for d, p in zip(data, planned_events)
        ]
    )


async def test_add_planned_event(
    client: TestClient, auth_headers: Dict[str, str]
) -> None:
    now = datetime.utcnow()
    resp = client.post(
        "/events/planned/add",
        data=json.dumps(
            {"name": "test", "timestamp": int(now.timestamp())},
        ),
        headers=auth_headers,
    )
    assert resp.status_code == 200
    data = resp.json()
    assert data["name"] == "test"
    assert data["datetime"] == int(now.timestamp())

    await PlannedEvent.filter(name="test").exists()


async def test_delete_planned_event(
    client: TestClient,
    auth_headers: Dict[str, str],
    planned_event: PlannedEvent,
) -> None:
    id_ = planned_event.id

    resp = client.post(
        "/events/planned/delete",
        data=json.dumps({"id": id_}),
        headers=auth_headers,
    )
    assert resp.status_code == 200

    assert not await PlannedEvent.filter(id=id_)


async def test_delete_planned_event__doesnt_exist(
    client: TestClient,
    auth_headers: Dict[str, str],
) -> None:
    resp = client.post(
        "/events/planned/delete",
        data=json.dumps({"id": 0}),
        headers=auth_headers,
    )
    assert resp.status_code == 400
    assert resp.text == "The planned event with id 0 doesn't exist."


@pytest.mark.parametrize(
    "field,val",
    [("timestamp", datetime.utcnow() + timedelta(minutes=1))],
)
async def test_update_planned_event(
    client: TestClient,
    auth_headers: Dict[str, str],
    planned_event: PlannedEvent,
    field: str,
    val: Any,
) -> None:
    if field == "timestamp":
        val = int(val.timestamp())
    resp = client.post(
        "/events/planned/update",
        data=json.dumps({"id": planned_event.id, field: val}),
        headers=auth_headers,
    )
    assert resp.status_code == 200
    event = await PlannedEvent.get(id=planned_event.id)
    data = resp.json()
    if field == "timestamp":
        assert data["datetime"] == val
        assert event.datetime == datetime.utcfromtimestamp(val).replace(
            tzinfo=timezone.utc
        )


async def test_update_planned_event__doesnt_exist(
    client: TestClient,
    auth_headers: Dict[str, str],
) -> None:
    resp = client.post(
        "/events/planned/update",
        data=json.dumps({"id": 0}),
        headers=auth_headers,
    )
    assert resp.status_code == 400
    assert resp.text == "The planned event with id 0 doesn't exist."


async def test_get_recurring_events(
    client: TestClient,
    recurring_events: List[RecurringEvent],
    auth_headers: Dict[str, str],
) -> None:
    resp = client.get("/events/recurring", headers=auth_headers)
    assert resp.status_code == 200
    data = resp.json()
    assert all(
        [
            d == convert_datetimes_for_json(await p.dict())
            for d, p in zip(data, recurring_events)
        ]
    )


async def test_add_recurring_event(
    client: TestClient, auth_headers: Dict[str, str]
) -> None:
    resp = client.post(
        "/events/recurring/add",
        data=json.dumps(
            {
                "name": "test",
                "on": [False] * 3 + [True] + [False] * 3,
                "every_n_weeks": 2,
            },
        ),
        headers=auth_headers,
    )
    assert resp.status_code == 200
    data = resp.json()
    assert data["name"] == "test"
    assert data["on"] == [False] * 3 + [True] + [False] * 3
    assert data["every_n_weeks"] == 2

    await RecurringEvent.filter(name="test").exists()


async def test_add_recurring_event__wrong_number_for_on(
    client: TestClient, auth_headers: Dict[str, str]
) -> None:
    resp = client.post(
        "/events/recurring/add",
        data=json.dumps(
            {
                "name": "test",
                "on": [False],
                "every_n_weeks": 2,
            },
        ),
        headers=auth_headers,
    )
    assert resp.status_code == 400
    assert resp.text == (
        "The `on` field must be a list of bools indicating if the event "
        "should happen on the corresponding day or not."
    )


async def test_delete_recurring_event(
    client: TestClient,
    auth_headers: Dict[str, str],
    recurring_event: RecurringEvent,
) -> None:
    id_ = recurring_event.id

    resp = client.post(
        "/events/recurring/delete",
        data=json.dumps({"id": id_}),
        headers=auth_headers,
    )
    assert resp.status_code == 200

    assert not await RecurringEvent.filter(id=id_)


async def test_delete_recurring_event__doesnt_exist(
    client: TestClient,
    auth_headers: Dict[str, str],
) -> None:
    resp = client.post(
        "/events/recurring/delete",
        data=json.dumps({"id": 0}),
        headers=auth_headers,
    )
    assert resp.status_code == 400
    assert resp.text == "The recurring event with id 0 doesn't exist."


@pytest.mark.parametrize(
    "field,val",
    [("on", "1111111"), ("every_n_weeks", 4)],
)
async def test_update_recurring_event(
    client: TestClient,
    auth_headers: Dict[str, str],
    recurring_event: RecurringEvent,
    field: str,
    val: Any,
) -> None:
    resp = client.post(
        "/events/recurring/update",
        data=json.dumps({"id": recurring_event.id, field: val}),
        headers=auth_headers,
    )
    assert resp.status_code == 200
    event = await RecurringEvent.get(id=recurring_event.id)
    data = resp.json()
    if field == "on":
        assert data["on"] == [True] * 7
        assert event.on == val
    else:
        assert data[field] == val
        assert getattr(event, field) == val


async def test_update_recurring_event__doesnt_exist(
    client: TestClient,
    auth_headers: Dict[str, str],
) -> None:
    resp = client.post(
        "/events/recurring/update",
        data=json.dumps({"id": 0}),
        headers=auth_headers,
    )
    assert resp.status_code == 400
    assert resp.text == "The recurring event with id 0 doesn't exist."


async def test_update_recurring_event__wrong_number_for_on(
    client: TestClient,
    auth_headers: Dict[str, str],
    recurring_event: RecurringEvent,
) -> None:
    resp = client.post(
        "/events/recurring/update",
        data=json.dumps({"id": recurring_event.id, "on": "0"}),
        headers=auth_headers,
    )
    assert resp.status_code == 400
    assert resp.text == (
        "The `on` field must be a list of bools indicating if the event "
        "should happen on the corresponding day or not."
    )
