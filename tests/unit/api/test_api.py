from starlette.testclient import TestClient


async def test_health_check(client: TestClient) -> None:
    resp = client.get("/")
    assert resp.status_code == 200
    assert resp.text == "Healthy!"
