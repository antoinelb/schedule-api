import json
from typing import Any, Dict, List

import pytest
from src.db import Project
from src.api.utils import convert_datetimes_for_json
from starlette.testclient import TestClient


async def test_get_projects(
    client: TestClient,
    projects: List[Project],
    auth_headers: Dict[str, str],
) -> None:
    resp = client.get("/projects", headers=auth_headers)
    assert resp.status_code == 200
    data = resp.json()
    assert all(
        [
            d == convert_datetimes_for_json(await p.dict())
            for d, p in zip(data, projects)
        ]
    )


async def test_add_project(
    client: TestClient, auth_headers: Dict[str, str]
) -> None:
    resp = client.post(
        "/projects/add",
        data=json.dumps(
            {"name": "test", "colour": 285},
        ),
        headers=auth_headers,
    )
    assert resp.status_code == 200
    data = resp.json()
    assert data["name"] == "test"
    assert data["colour"] == 285

    await Project.filter(name="test").exists()


async def test_add_project__already_exists(
    client: TestClient, auth_headers: Dict[str, str], project: Project
) -> None:
    resp = client.post(
        "/projects/add",
        data=json.dumps(
            {"name": project.name, "colour": 285},
        ),
        headers=auth_headers,
    )
    assert resp.status_code == 400
    assert resp.text == f"There already is a project named {project.name}."


@pytest.mark.parametrize(
    "field,val",
    [("name", "new"), ("colour", 100)],
)
async def test_update_project(
    client: TestClient,
    auth_headers: Dict[str, str],
    project: Project,
    field: str,
    val: Any,
) -> None:
    resp = client.post(
        "/projects/update",
        data=json.dumps({"id": project.id, field: val}),
        headers=auth_headers,
    )
    assert resp.status_code == 200
    project = await Project.get(id=project.id)
    data = resp.json()
    assert data[field] == val
    assert getattr(project, field) == val


async def test_update_project__doesnt_exist(
    client: TestClient,
    auth_headers: Dict[str, str],
) -> None:
    resp = client.post(
        "/projects/update",
        data=json.dumps({"id": 0}),
        headers=auth_headers,
    )
    assert resp.status_code == 400
    assert resp.text == "There is no project with id 0."


async def test_delete_project(
    client: TestClient,
    auth_headers: Dict[str, str],
    project: Project,
) -> None:
    resp = client.post(
        "/projects/delete",
        data=json.dumps({"id": project.id}),
        headers=auth_headers,
    )
    assert resp.status_code == 200
    assert not await Project.filter(id=project.id).exists()


async def test_delete_project__doesnt_exist(
    client: TestClient,
    auth_headers: Dict[str, str],
) -> None:
    resp = client.post(
        "/projects/delete",
        data=json.dumps({"id": 0}),
        headers=auth_headers,
    )
    assert resp.status_code == 400
    assert resp.text == "There is no project with id 0."
