from pytest_mock import MockerFixture
from src.cli import init_cli, run_cli
from typer import Typer
from typer.testing import CliRunner


def test_init_cli() -> None:
    cli = init_cli()
    assert isinstance(cli, Typer)


def test_run_cli(mocker: MockerFixture) -> None:
    init_cli = mocker.patch("src.cli.init_cli")
    run_cli()
    init_cli.assert_called_once()


def test_run(
    mocker: MockerFixture,
    runner: CliRunner,
    cli: Typer,
) -> None:
    run_server = mocker.patch("src.cli.app.run_server")

    resp = runner.invoke(cli)
    assert resp.exit_code == 0
    run_server.assert_called_once()
