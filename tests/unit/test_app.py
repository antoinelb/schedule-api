import os
import threading
import time

import httpx
import pytest
from pytest_mock import MockerFixture
from src import config
from src.app import create_app, get_middleware, run_server
from starlette.applications import Starlette


@pytest.mark.parametrize("debug", [False, True])
def test_get_middleware(debug: bool) -> None:
    config.DEBUG = debug
    middleware = get_middleware()
    if debug:
        assert str(middleware[0]).startswith(
            "Middleware(CORSMiddleware, allow_origins=['*']"
        )
    else:
        assert str(middleware[0]).startswith(
            "Middleware(CORSMiddleware, "
            f"allow_origins=['https://{config.DOMAIN}']"
        )

    assert str(middleware[1]).startswith("Middleware(AuthMiddleware")


def test_create_app(mocker: MockerFixture) -> None:
    mocker.patch("src.app.init_logging")
    mocker.patch("src.app.logger")
    api = mocker.patch("src.app.api")
    api.add_routes = lambda x: x

    app = create_app()
    assert isinstance(app, Starlette)


@pytest.mark.no_db
async def test_create_app__unitialized(mocker: MockerFixture) -> None:
    mocker.patch("src.app.init_logging")
    mocker.patch("src.app.logger")
    api = mocker.patch("src.app.api")
    api.add_routes = lambda x: x

    app = create_app()
    assert isinstance(app, Starlette)


def test_run_server() -> None:
    timeout = 1
    try_every = 0.1

    host = "127.0.0.1"
    port = 8001

    os.environ["HOST"] = host
    os.environ["PORT"] = str(port)
    os.environ["RELOAD"] = "0"
    os.environ["DEBUG"] = "0"

    thread = threading.Thread(target=run_server)
    thread.daemon = True
    thread.start()

    time_ = 0.0
    while time_ < timeout:
        try:
            resp = httpx.get(f"http://{host}:{port}/ping")
            assert resp.status_code == 200
            break
        except httpx.ConnectError:
            time_ = time_ + try_every
            time.sleep(try_every)
