from src.db import Project, User


async def test_project__str(project: Project, user: User) -> None:
    assert str(project) == f"<project {project.name} for user {user.email}>"


async def test_project__dict(project: Project) -> None:
    project_ = await project.dict()
    for field in (
        "id",
        "user_",
        "name",
        "colour",
        "created_on",
        "last_modified",
    ):
        assert project_[field] == getattr(project, field)
    assert len(project_) == 6
