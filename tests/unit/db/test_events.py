from src.db import Event, PlannedEvent, RecurringEvent, User


async def test_event__str(event: Event, user: User) -> None:
    assert str(event) == f"<event {event.name} for user {user.email}>"


async def test_event__dict(event: Event) -> None:
    event_ = await event.dict()
    for field in (
        "id",
        "user_",
        "name",
        "datetime",
        "created_on",
        "last_modified",
    ):
        assert event_[field] == getattr(event, field)
    assert len(event_) == 6


async def test_planned_event__str(
    planned_event: PlannedEvent, user: User
) -> None:
    assert (
        str(planned_event)
        == f"<planned event {planned_event.name} for user {user.email}>"
    )


async def test_planned_event__dict(planned_event: PlannedEvent) -> None:
    event_ = await planned_event.dict()
    for field in (
        "id",
        "user_",
        "name",
        "datetime",
        "created_on",
        "last_modified",
    ):
        assert event_[field] == getattr(planned_event, field)
    assert len(event_) == 6


async def test_recurring_event__str(
    recurring_event: RecurringEvent, user: User
) -> None:
    assert (
        str(recurring_event)
        == f"<recurring event {recurring_event.name} for user {user.email}>"
    )


async def test_recurring_event__dict(recurring_event: RecurringEvent) -> None:
    recurring_event_ = await recurring_event.dict()
    for field in (
        "id",
        "user_",
        "name",
        "every_n_weeks",
        "created_on",
        "last_modified",
    ):
        assert recurring_event_[field] == getattr(recurring_event, field)
    assert recurring_event_["on"] == [bool(int(d)) for d in recurring_event.on]
    assert len(recurring_event_) == 7
