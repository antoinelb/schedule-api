from src.db import User


async def test_user__str(user: User, admin: User) -> None:
    assert str(user) == f"<user {user.email}>"
    assert str(admin) == f"<admin user {admin.email}>"


async def test_user__dict(user: User) -> None:
    user_ = await user.dict()
    for field in (
        "email",
        "is_admin",
    ):
        assert user_[field] == getattr(user, field)
    assert len(user_) == 2
