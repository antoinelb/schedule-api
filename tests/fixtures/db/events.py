from datetime import datetime, timedelta
from typing import List
import pytest
from src.db import Event, PlannedEvent, RecurringEvent, User

from .users import *  # noqa


@pytest.fixture
async def event(user: User) -> Event:
    return await Event.create(
        user_=user.email, name="test", datetime=datetime.utcnow()
    )


@pytest.fixture
async def events(user: User) -> List[Event]:
    return [
        await Event.create(
            user_=user.email,
            name=f"test_{i}",
            datetime=datetime.utcnow() + timedelta(minutes=i + 1),
        )
        for i in range(3)
    ]


@pytest.fixture
async def planned_event(user: User) -> PlannedEvent:
    return await PlannedEvent.create(
        user_=user.email,
        name="test",
        datetime=datetime.utcnow() + timedelta(days=3),
    )


@pytest.fixture
async def planned_events(user: User) -> List[PlannedEvent]:
    return [
        await PlannedEvent.create(
            user_=user.email,
            name=f"test_{i}",
            datetime=datetime.utcnow() + timedelta(days=i + 1),
        )
        for i in range(3)
    ]


@pytest.fixture
async def recurring_event(user: User) -> RecurringEvent:
    return await RecurringEvent.create(
        user_=user.email, name="test", on="1110000", every_n_weeks=1
    )


@pytest.fixture
async def recurring_events(user: User) -> List[RecurringEvent]:
    return [
        await RecurringEvent.create(
            user_=user.email,
            name=f"test_{i}",
            on="1110000",
            every_n_weeks=i + 1,
        )
        for i in range(3)
    ]
