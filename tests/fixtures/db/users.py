from typing import List

import pytest
from src.db import User

from .db import *  # noqa


@pytest.fixture
def user() -> User:
    return User(email="test@test.com", is_admin=False)


@pytest.fixture
def users() -> List[User]:
    return [User(email=f"test_{i}@test.com", is_admin=False) for i in range(3)]


@pytest.fixture
def admin() -> User:
    return User(email="admin@test.com", is_admin=True)
