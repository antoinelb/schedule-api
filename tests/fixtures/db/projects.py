import pytest
from typing import List
from src.db import Project, User

from .users import *  # noqa


@pytest.fixture
async def project(user: User) -> Project:
    return await Project.create(user_=user.email, name="test", colour=285)


@pytest.fixture
async def projects(user: User) -> List[Project]:
    return [
        await Project.create(
            user_=user.email, name=f"test_{i}", colour=285 + i
        )
        for i in range(3)
    ]
