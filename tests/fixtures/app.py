import pytest
from src.app import create_app
from starlette.applications import Starlette
from starlette.requests import Request
from starlette.responses import PlainTextResponse, Response
from starlette.testclient import TestClient


@pytest.fixture
def app() -> Starlette:
    async def secure_ping(req: Request) -> Response:
        return PlainTextResponse(
            f"Secure pong for user {req.scope['user'].email}!"
        )

    app = create_app()
    app.add_route("/secure", secure_ping, methods=["GET"])
    return app


@pytest.fixture
def client(app: Starlette) -> TestClient:
    return TestClient(app)
