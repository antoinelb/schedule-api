from datetime import datetime, timedelta
from typing import Dict

import jwt
import pytest
from pytest_mock import MockerFixture  # type: ignore
from src import config
from src.db import User

from .db import *  # noqa


@pytest.fixture
def access_token(user: User) -> str:
    payload = {
        "user": {"email": user.email, "is_admin": user.is_admin},
        "aud": config.TOKEN_AUD,
        "iat": datetime.utcnow(),
        "exp": datetime.utcnow() + timedelta(minutes=1),
    }
    return jwt.encode(payload, str(config.TOKEN_KEY))  # type: ignore


@pytest.fixture
def auth_headers(mocker: MockerFixture, user: User) -> Dict[str, str]:
    mocker.patch("src.auth.verify_token", return_value=user)
    return {"access-token": "test-token"}


@pytest.fixture
def admin_auth_headers(mocker: MockerFixture, admin: User) -> Dict[str, str]:
    mocker.patch("src.auth.verify_token", return_value=admin)
    return {"access-token": "test-token"}
