from typing import List

import uvicorn
from starlette.applications import Starlette
from starlette.middleware import Middleware
from starlette.middleware.cors import CORSMiddleware

from . import api, auth, config, db
from .logging import init_logging, logger


def get_middleware() -> List[Middleware]:
    middleware = [
        Middleware(auth.AuthMiddleware),
    ]

    if config.DEBUG:
        allowed_origins = ["*"]
    else:
        allowed_origins = [f"https://{config.DOMAIN}"]

    return [
        Middleware(
            CORSMiddleware,
            allow_origins=allowed_origins,
            allow_credentials=True,
            allow_methods=["*"],
            allow_headers=["*"],
        )
    ] + middleware


def create_app() -> Starlette:
    init_logging()

    app = Starlette(
        debug=config.DEBUG,
        routes=api.get_routes(),
        middleware=get_middleware(),
    )
    app.router.on_startup.append(db.init_db)
    app.router.on_shutdown.append(db.close_db)

    logger.info("App started.")
    if config.DEBUG:
        logger.warning("Running in debug mode")

    return app


def run_server() -> None:
    init_logging()

    logger.info(
        f"Starting app in {'debug' if config.DEBUG else 'production'} mode "
        f"on port {config.PORT}"
    )

    uvicorn.run(
        "src.server:app",
        host=config.HOST,
        port=config.PORT,
        reload=config.RELOAD,
        log_level="debug" if config.DEBUG else "info",
        access_log=True,
    )
