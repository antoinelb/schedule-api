from typing import List

from starlette.requests import Request
from starlette.responses import PlainTextResponse, Response
from starlette.routing import BaseRoute, Route, Mount

from . import projects, events


def get_routes() -> List[BaseRoute]:
    return [
        Route("/", endpoint=health_check, methods=["GET"]),
        Mount("/projects", routes=projects.get_routes()),
        Mount("/events", routes=events.get_routes()),
    ]


async def health_check(_: Request) -> Response:
    return PlainTextResponse("Healthy!")
