from datetime import datetime
from typing import List, Optional, Dict, Any

from starlette.requests import Request
from starlette.responses import Response, PlainTextResponse
from starlette.routing import Route
from tortoise.exceptions import DoesNotExist

from .utils import with_json_params, JSONResponse
from ..db import Event, PlannedEvent, RecurringEvent


def get_routes() -> List[Route]:
    return [
        Route("/", get_events, methods=["GET"]),
        Route("/add", add_event, methods=["POST"]),
        Route("/delete", delete_event, methods=["POST"]),
        Route("/planned", get_planned_events, methods=["GET"]),
        Route("/planned/add", add_planned_event, methods=["POST"]),
        Route("/planned/delete", delete_planned_event, methods=["POST"]),
        Route("/planned/update", update_planned_event, methods=["POST"]),
        Route("/recurring", get_recurring_events, methods=["GET"]),
        Route("/recurring/add", add_recurring_event, methods=["POST"]),
        Route("/recurring/delete", delete_recurring_event, methods=["POST"]),
        Route("/recurring/update", update_recurring_event, methods=["POST"]),
    ]


async def get_events(req: Request) -> Response:
    events = await Event.filter(user_=req.scope["user"].email).all()
    return JSONResponse([await event.dict() for event in events])


@with_json_params(args=["name", "timestamp"])
async def add_event(req: Request, name: str, timestamp: int) -> Response:
    event = await Event.create(
        user_=req.scope["user"].email,
        name=name,
        datetime=datetime.utcfromtimestamp(timestamp),
    )
    return JSONResponse(await event.dict())


@with_json_params(args=["id"])
async def delete_event(_: Request, id: int) -> Response:
    try:
        event = await Event.get(id=id)
    except DoesNotExist:
        return PlainTextResponse(
            f"The event with id {id} doesn't exist.", status_code=400
        )
    await event.delete()
    return PlainTextResponse("")


async def get_planned_events(req: Request) -> Response:
    events = await PlannedEvent.filter(user_=req.scope["user"].email).all()
    return JSONResponse([await event.dict() for event in events])


@with_json_params(args=["name", "timestamp"])
async def add_planned_event(
    req: Request, name: str, timestamp: int
) -> Response:
    event = await PlannedEvent.create(
        user_=req.scope["user"].email,
        name=name,
        datetime=datetime.utcfromtimestamp(timestamp),
    )
    return JSONResponse(await event.dict())


@with_json_params(args=["id"])
async def delete_planned_event(_: Request, id: int) -> Response:
    try:
        event = await PlannedEvent.get(id=id)
    except DoesNotExist:
        return PlainTextResponse(
            f"The planned event with id {id} doesn't exist.", status_code=400
        )
    await event.delete()
    return PlainTextResponse("")


@with_json_params(args=["id"], opt_args=["timestamp"])
async def update_planned_event(
    _: Request,
    id: int,
    timestamp: Optional[int] = None,
) -> Response:
    try:
        event = await PlannedEvent.get(id=id)
    except DoesNotExist:
        return PlainTextResponse(
            f"The planned event with id {id} doesn't exist.", status_code=400
        )

    update = (
        {"datetime": datetime.utcfromtimestamp(timestamp)}
        if timestamp is not None
        else {}
    )

    event = await event.update_from_dict(update)
    await event.save()

    return JSONResponse(await event.dict())


async def get_recurring_events(req: Request) -> Response:
    events = await RecurringEvent.filter(user_=req.scope["user"].email).all()
    return JSONResponse([await event.dict() for event in events])


@with_json_params(args=["name", "on", "every_n_weeks"])
async def add_recurring_event(
    req: Request, name: str, on: List[bool], every_n_weeks: int
) -> Response:
    if len(on) != 7:
        return PlainTextResponse(
            "The `on` field must be a list of bools indicating if the event "
            "should happen on the corresponding day or not.",
            400,
        )
    event = await RecurringEvent.create(
        user_=req.scope["user"].email,
        name=name,
        every_n_weeks=every_n_weeks,
        on="".join("1" if day else "0" for day in on),
    )
    print(event.on)
    return JSONResponse(await event.dict())


@with_json_params(args=["id"])
async def delete_recurring_event(_: Request, id: int) -> Response:
    try:
        event = await RecurringEvent.get(id=id)
    except DoesNotExist:
        return PlainTextResponse(
            f"The recurring event with id {id} doesn't exist.", status_code=400
        )
    await event.delete()
    return PlainTextResponse("")


@with_json_params(args=["id"], opt_args=["on", "every_n_weeks"])
async def update_recurring_event(
    _: Request,
    id: int,
    on: Optional[List[bool]] = None,
    every_n_weeks: Optional[int] = None,
) -> Response:
    try:
        event = await RecurringEvent.get(id=id)
    except DoesNotExist:
        return PlainTextResponse(
            f"The recurring event with id {id} doesn't exist.", status_code=400
        )

    if on is not None and len(on) != 7:
        return PlainTextResponse(
            "The `on` field must be a list of bools indicating if the event "
            "should happen on the corresponding day or not.",
            400,
        )

    update: Dict[str, Any] = {
        **(
            {"every_n_weeks": every_n_weeks}
            if every_n_weeks is not None
            else {}
        ),
        **(
            {"on": "".join("1" if day else "0" for day in on)}
            if on is not None
            else {}
        ),
    }

    event = await event.update_from_dict(update)
    await event.save()

    return JSONResponse(await event.dict())
