from typing import List, Optional, Dict, Any

from starlette.requests import Request
from starlette.responses import Response, PlainTextResponse
from starlette.routing import Route
from tortoise.exceptions import IntegrityError, DoesNotExist

from .utils import with_json_params, JSONResponse
from ..db import Project


def get_routes() -> List[Route]:
    return [
        Route("/", get_projects, methods=["GET"]),
        Route("/add", add_project, methods=["POST"]),
        Route("/update", update_project, methods=["POST"]),
        Route("/delete", delete_project, methods=["POST"]),
    ]


async def get_projects(req: Request) -> Response:
    projects = await Project.filter(user_=req.scope["user"].email).all()
    return JSONResponse([await project.dict() for project in projects])


@with_json_params(args=["name", "colour"])
async def add_project(req: Request, name: str, colour: int) -> Response:
    try:
        project = await Project.create(
            user_=req.scope["user"].email, name=name, colour=colour
        )
    except IntegrityError:
        return PlainTextResponse(
            f"There already is a project named {name}.", 400
        )
    return JSONResponse(await project.dict())


@with_json_params(args=["id"], opt_args=["name", "colour"])
async def update_project(
    req: Request,
    id: int,  # pylint: disable=redefined-builtin
    name: Optional[str] = None,
    colour: Optional[int] = None,
) -> Response:
    try:
        project = await Project.get(user_=req.scope["user"].email, id=id)
    except DoesNotExist:
        return PlainTextResponse(f"There is no project with id {id}.", 400)

    update: Dict[str, Any] = {}
    if name is not None:
        update["name"] = name
    if colour is not None:
        update["colour"] = colour

    await project.update_from_dict(update)
    await project.save()

    return JSONResponse(await project.dict())


@with_json_params(args=["id"])
async def delete_project(
    req: Request,
    id: int,  # pylint: disable=redefined-builtin
) -> Response:
    try:
        project = await Project.get(user_=req.scope["user"].email, id=id)
    except DoesNotExist:
        return PlainTextResponse(f"There is no project with id {id}.", 400)
    await project.delete()
    return PlainTextResponse("")
