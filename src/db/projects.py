from typing import Any, Dict

from tortoise import fields
from tortoise.models import Model


class Project(Model):

    id = fields.IntField(pk=True)
    user_ = fields.CharField(64, index=True)
    name = fields.CharField(32, index=True)
    colour = fields.IntField()
    created_on = fields.DatetimeField(auto_now_add=True)
    last_modified = fields.DatetimeField(auto_now=True)

    class Meta:
        table = "projects"
        unique_together = ("user_", "name")

    def __str__(self: "Project") -> str:
        return f"<project {self.name} for user {self.user_}>"

    async def dict(self: "Project") -> Dict[str, Any]:
        return {
            field: val
            for field, val in self.__dict__.items()
            if not field.startswith("_")
        }
