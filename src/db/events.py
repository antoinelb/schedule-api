from typing import Any, Dict

from tortoise import fields
from tortoise.models import Model


class Event(Model):
    id = fields.IntField(pk=True)
    user_ = fields.CharField(64, index=True)
    name = fields.CharField(32, index=True)
    datetime = fields.DatetimeField()
    created_on = fields.DatetimeField(auto_now_add=True)
    last_modified = fields.DatetimeField(auto_now=True)

    class Meta:
        table = "events"

    def __str__(self: "Event") -> str:
        return f"<event {self.name} for user {self.user_}>"

    async def dict(self: "Event") -> Dict[str, Any]:
        return {
            field: val
            for field, val in self.__dict__.items()
            if not field.startswith("_")
        }


class PlannedEvent(Model):
    id = fields.IntField(pk=True)
    user_ = fields.CharField(64, index=True)
    name = fields.CharField(32, index=True)
    datetime = fields.DatetimeField()
    created_on = fields.DatetimeField(auto_now_add=True)
    last_modified = fields.DatetimeField(auto_now=True)

    class Meta:
        table = "planned_events"

    def __str__(self: "PlannedEvent") -> str:
        return f"<planned event {self.name} for user {self.user_}>"

    async def dict(self: "PlannedEvent") -> Dict[str, Any]:
        return {
            field: val
            for field, val in self.__dict__.items()
            if not field.startswith("_")
        }


class RecurringEvent(Model):
    id = fields.IntField(pk=True)
    user_ = fields.CharField(64, index=True)
    name = fields.CharField(32, index=True)
    on = fields.CharField(7)
    every_n_weeks = fields.IntField()
    created_on = fields.DatetimeField(auto_now_add=True)
    last_modified = fields.DatetimeField(auto_now=True)

    class Meta:
        table = "recurring_events"

    def __str__(self: "RecurringEvent") -> str:
        return f"<recurring event {self.name} for user {self.user_}>"

    async def dict(self: "RecurringEvent") -> Dict[str, Any]:
        return {
            field: ([bool(int(day)) for day in val] if field == "on" else val)
            for field, val in self.__dict__.items()
            if not field.startswith("_")
        }
