from tortoise import Tortoise

from .. import config
from .defaults import add_default_db_data
from .projects import Project
from .events import Event, PlannedEvent, RecurringEvent

db_url = (
    f"postgres://{config.DB_USER}:{config.DB_PASS}"
    f"@{config.DB_HOST}:{config.DB_PORT}/{config.DB_NAME}"
)
modules = {
    "models": [
        "aerich.models",
        "src.db.projects",
        "src.db.events",
    ]
}
db_config = {
    "connections": {"default": db_url},
    "apps": {
        "models": {
            **{
                "default_connection": "default",
            },
            **modules,  # type: ignore
        }
    },
    "use_tz": False,
}


async def init_db() -> None:
    await Tortoise.init(db_url=db_url, modules=modules)
    await add_default_db_data()


async def close_db() -> None:
    await Tortoise.close_connections()


async def empty_db() -> None:
    await Project.all().delete()
    await Event.all().delete()
    await PlannedEvent.all().delete()
    await RecurringEvent.all().delete()
