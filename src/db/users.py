from dataclasses import dataclass
from typing import Any, Dict


@dataclass
class User:
    email: str
    is_admin: bool

    def __str__(self: "User") -> str:
        if self.is_admin:
            return f"<admin user {self.email}>"
        else:
            return f"<user {self.email}>"

    async def dict(self: "User") -> Dict[str, Any]:
        return {
            "email": self.email,
            "is_admin": self.is_admin,
        }
