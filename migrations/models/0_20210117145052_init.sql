-- upgrade --
CREATE TABLE IF NOT EXISTS "aerich" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "version" VARCHAR(255) NOT NULL,
    "app" VARCHAR(20) NOT NULL,
    "content" TEXT NOT NULL
);
CREATE TABLE IF NOT EXISTS "projects" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "user_" VARCHAR(64) NOT NULL,
    "name" VARCHAR(32) NOT NULL,
    "color" INT NOT NULL,
    "created_on" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "last_modified" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT "uid_projects_user__270efb" UNIQUE ("user_", "name")
);
CREATE INDEX IF NOT EXISTS "idx_projects_user__0448e5" ON "projects" ("user_");
CREATE INDEX IF NOT EXISTS "idx_projects_name_7b5b92" ON "projects" ("name");
CREATE TABLE IF NOT EXISTS "events" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "user_" VARCHAR(64) NOT NULL,
    "name" VARCHAR(32) NOT NULL,
    "datetime" TIMESTAMPTZ NOT NULL,
    "created_on" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "last_modified" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP
);
CREATE INDEX IF NOT EXISTS "idx_events_user__e08de6" ON "events" ("user_");
CREATE INDEX IF NOT EXISTS "idx_events_name_c84318" ON "events" ("name");
CREATE TABLE IF NOT EXISTS "planned_events" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "user_" VARCHAR(64) NOT NULL,
    "name" VARCHAR(32) NOT NULL,
    "datetime" TIMESTAMPTZ NOT NULL,
    "created_on" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "last_modified" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP
);
CREATE INDEX IF NOT EXISTS "idx_planned_eve_user__843cfd" ON "planned_events" ("user_");
CREATE INDEX IF NOT EXISTS "idx_planned_eve_name_78f81b" ON "planned_events" ("name");
CREATE TABLE IF NOT EXISTS "recurring_events" (
    "id" SERIAL NOT NULL PRIMARY KEY,
    "user_" VARCHAR(64) NOT NULL,
    "name" VARCHAR(32) NOT NULL,
    "on" VARCHAR(7) NOT NULL,
    "every_n_weeks" INT NOT NULL,
    "created_on" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP,
    "last_modified" TIMESTAMPTZ NOT NULL  DEFAULT CURRENT_TIMESTAMP
);
CREATE INDEX IF NOT EXISTS "idx_recurring_e_user__dd79b9" ON "recurring_events" ("user_");
CREATE INDEX IF NOT EXISTS "idx_recurring_e_name_d6de63" ON "recurring_events" ("name");
