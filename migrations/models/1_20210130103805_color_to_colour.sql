-- upgrade --
ALTER TABLE "projects" RENAME COLUMN "color" TO "colour";
-- downgrade --
ALTER TABLE "projects" RENAME COLUMN "colour" TO "color";
